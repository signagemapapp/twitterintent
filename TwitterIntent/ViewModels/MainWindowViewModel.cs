﻿using System;
using System.Drawing;

using Livet;

using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;

namespace TwitterIntent.ViewModels
{
    public class MainWindowViewModel : ViewModel
    {
        static string TWITTER_INTENT_URI = "https://twitter.com/intent/tweet?text=";

        #region TweetText変更通知プロパティ
        private string _TweetText;

        public string TweetText
        {
            get
            { return _TweetText; }
            set
            {
                if (_TweetText == value)
                    return;
                _TweetText = value;
                RaisePropertyChanged();
            }
        }
        #endregion


        #region QRCodeImage変更通知プロパティ
        private Bitmap _QRCodeImage;

        public Bitmap QRCodeImage
        {
            get
            { return _QRCodeImage; }
            set
            {
                if (_QRCodeImage == value)
                    return;
                _QRCodeImage = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        public void Initialize()
        {
            TweetText = "ここにツイートを書いてくれ！";
        }

        // ボタンをクリックしたときに呼びだされるメソッド
        public void QRCodeGenerate()
        {
            // TweetTextをURLエンコードしたものと, Twitter intentのURLをあわせる
            var encodedStr = TWITTER_INTENT_URI + Uri.EscapeUriString(TweetText);


            // QRコード生成の準備
            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = qrEncoder.Encode(encodedStr);
            GraphicsRenderer renderer = new GraphicsRenderer(new FixedModuleSize(5, QuietZoneModules.Two), System.Drawing.Brushes.Black, System.Drawing.Brushes.White);

            // Bitmapオブジェクトの準備
            Bitmap qrCodeImg = new Bitmap(200, 200);
            using (var g = Graphics.FromImage(qrCodeImg))
            {
                new GraphicsRenderer(new FixedCodeSize(200, QuietZoneModules.Two)).Draw(g, qrCode.Matrix);
            }

            // QRコード画像のオブジェクトをViewに反映
            QRCodeImage = qrCodeImg;
        }
    }
}
